# !!! WARNING !!!
# the execution of this script might crash blender 2.91
# SAVE BEFORE RUN!

import bpy, math, mathutils, random

COLLECTIONS = [ 'flying_gears', 'gears_lvl3', 'gears_lvl2', 'gears_lvl1', 'gears_lvl0', 'gears_lvl-1' ]
RENDER_FOLDER = '//../gear_exports/singles/'
#CENTER = bpy.data.objects['center']
CENTER = None

INIT = []

# collecting info
for c in COLLECTIONS:
    coll = bpy.data.collections[c]
    for o in coll.objects:
        if o.hide_render == False:
            INIT.append( { 'obj': o, 'pos': o.location, 'visible': o.hide_viewport } )
            o.hide_viewport = True
            o.hide_render = True

# exporting
gid = 0
for i in INIT:
    o = i['obj']
    if CENTER != None:
        o.location = CENTER.location
    o.hide_render = False
    n = str(gid)
    while len(n) < 3:
        n = '0'+n
    bpy.context.scene.render.filepath = RENDER_FOLDER + 'gear_' + n + '.svg' 
    bpy.ops.render.render()
    o.hide_render = True
    if CENTER != None:
        o.location = i['pos']
    gid += 1

# reseting scene
for i in INIT:
    o = i['obj']
    print( o.name )
    o.hide_viewport = i['visible']
    o.hide_render = False