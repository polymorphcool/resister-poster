from lxml import etree
import os

FOLDER = 'singles'
OUPUT_FILE = 'all_gears.svg'

files = os.scandir(path=FOLDER)
gears = []
unique_names = {}

for f in files:
	if f.is_file() and f.name[-4:] == '.svg':
		name = f.name[:f.name.find('.svg')]
		parts = name.split( '_' )
		if len(parts) > 2:
			num = parts[1]
			while len(num) < 3:
				num = '0'+num
			parts[1] = num
			name = ''
			for i in range(0,2):
				if i > 0:
					name += '_'
				name += parts[i]
		if not name in unique_names:
			unique_names[ name ] = 1
		else:
			unique_names[ name ] += 1
		gears.append( { 'path': os.path.join( FOLDER, f.name ), 'name': name } )

for un in unique_names:
	if unique_names[un] > 1:
		i = 0
		for g in gears:
			if g['name'] == un:
				n = str(i)
				while len(n) < 3:
					n = '0'+n
				g['name'] += '_'+n
				i += 1

gears.sort(key=lambda item: item.get("name"))
gears = list( reversed( gears ) )

output_tmpl = '<?xml version=\'1.0\'?><svg xmlns="http://www.w3.org/2000/svg" xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape" height="1123" version="1.1" width="794"></svg>'
output_root = etree.fromstring( output_tmpl )
output_tree = etree.ElementTree( output_root )

for g in gears:
	
	with open( g['path'], 'r' ) as f:
		
		svg = etree.parse( f )
		root = svg.getroot()
		
		svg_ns = '{' + root.nsmap[None] + '}' 				# getting xmlns attr (standard svg namespace)
		inkscape_ns = '{' + root.nsmap['inkscape'] + '}' 	# getting xmlns:inkscape attr!
		
		# both namespaces must used to find elements in an svg:
		# all attributes NOT prefixed by 'inkscape:' can be used simply
		# all attributes prefixed by 'inkscape:' must be prefixed by 'inkscape_ns'
		
		layers = svg.findall( ".//"+svg_ns+"g[@"+inkscape_ns+"groupmode='layer']" )
		
		if len( layers ) > 0:
			nlayer = etree.SubElement( output_root, 'g' )
			nlayer.set( inkscape_ns+'label', g['name'] )
			nlayer.set( inkscape_ns+'groupmode', 'layer' )
			grp = etree.SubElement( output_root, 'g' )
			for l in layers:
				paths = l.findall( svg_ns+"path" )
				for p in paths:
					grp.append( p )
			nlayer.append( grp )
			output_root.append( nlayer )

output_tree.write( OUPUT_FILE )
