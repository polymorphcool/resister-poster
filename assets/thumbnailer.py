#!/usr/bin/python3

# sudo pip3 install svglib
# sudo apt-get install python3-renderpm

import os, collections
import PIL
from PIL import Image,ImageEnhance
from svglib.svglib import svg2rlg
from reportlab.graphics import renderPM
 
WEB_THUMB = 			[None,200]
SOURCE_FOLDER = 		"gear_exports"
TMP_FILE = 				"__thumbanil.tmp.png"
TABLE_COLUMNS = 		5
GENERATE_MARKDOWN = 	True
MARKDOWN_TITLE_PREFIX = "### assets/"
MARKDOWN_PATH_PREFIX = "assets"
MARKDOWN_FILE = 		"thumbnails.md"
MARKDOWN_PATH_RPLC = 	[ './', 'https://gitlab.com/polymorphcool/resister-poster/-/raw/master/' ]

def image_thumb( filename, src, dst ):
	
	try:
		
		drawing = svg2rlg( src )
		renderPM.drawToFile(drawing, TMP_FILE, fmt='PNG')
		
		img = Image.open( TMP_FILE )
		thumb = [WEB_THUMB[0],WEB_THUMB[1]]
		box = [0,0,img.size[0],img.size[1]]
		if thumb[0] == None:
			thumb[0] = int(thumb[1] / img.size[1] * img.size[0])
		elif thumb[1] == None:
			thumb[1] = int(thumb[0] / img.size[0] * img.size[1])
		
		img = img.resize( thumb, resample=PIL.Image.BICUBIC, box=box )
		img.save( dst )
		
		return dst
		
	except Exception as e:
		
		print( 'failed! ', src )
		print( e )
		return None

def get_src( svg ):
	return svg['src']

def get_path( svg ):
	return svg['path']

svgs = []
pngs = []

for root, dirs, files in os.walk(".", topdown=True):
	for f in files:
		if f[-4:] == ".svg":
			path = os.path.normpath(root)
			path = path.split(os.sep)
			svgs.append( { 'folder': path[len(path)-1], 'filename': f, 'src': os.path.join( root, f ), 'dst': os.path.join( root, f.replace('.svg','.png') ) } )

svgs.sort(key=get_src)

folders = {}
for svg in svgs:
	png = image_thumb( svg['filename'], svg['src'], svg['dst'] )
	if png != None:
		print( png, "generated" )
		if not svg['folder'] in folders:
			folders[svg['folder']] = []
		info = { 'folder': svg['folder'], 'name': svg['filename'], 'path': png }
		folders[svg['folder']].append( info )
		pngs.append( info )

folders = collections.OrderedDict(sorted(folders.items()))

if GENERATE_MARKDOWN:
	md = ''
	columni = 0
	for folder in folders:
		
		columni = 0
		folders[folder].sort(key=get_path)
		
		png_count = len( folders[folder] )
		if png_count == 0:
			continue
		
		if md != '':
			md += '\n\n'
		md += MARKDOWN_TITLE_PREFIX + folder + "\n\n"
		
		if png_count == 1:
			png = folders[folder][0]
			ppath = os.path.join( MARKDOWN_PATH_RPLC[1], MARKDOWN_PATH_PREFIX, png['path'].replace(MARKDOWN_PATH_RPLC[0],'')).replace(' ','%20')
			md += "!["+ png['name'] +"](" + ppath + ")["+ png['name'] +"](" + ppath.replace('.png','.svg') + ")"
		else:
			# starting the table
			ccount = png_count
			if ccount > TABLE_COLUMNS:
				ccount = TABLE_COLUMNS
			for i in range(0,ccount):
				md += " _ "
				if i < ccount-1:
					md += " | "
			md += '\n'
			for i in range(0,ccount):
				md += " --- "
				if i < ccount-1:
					md += " | "
			md += '\n'
			for png in folders[folder]:
				ppath = os.path.join( MARKDOWN_PATH_RPLC[1], MARKDOWN_PATH_PREFIX, png['path'].replace(MARKDOWN_PATH_RPLC[0],'')).replace(' ','%20')
				md += "!["+ png['name'] +"](" + ppath + ")["+ png['name'] +"](" + ppath.replace('.png','.svg') + ")"
				columni += 1
				if columni < TABLE_COLUMNS:
					md += " | "
				elif columni == TABLE_COLUMNS:
					md += "\n"
					columni = 0
		
	md += "\n"
	f = open( MARKDOWN_FILE, "w" )
	f.write( md )
	f.close()

try:
	os.remove(TMP_FILE) 
except Exception as e:
	pass
